const path = require('path');
const webpack = require('webpack');

const buildDirectory = path.join(__dirname, 'dist');
const config = {
    entry: './lib/main.jsx',
    devServer: {
        hot: true,
        inline: true,
        port: 7700,
        historyApiFallback: true
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    output: {
        path: path.resolve(buildDirectory),
        filname: 'app.js',
        publicPath: 'http://localhost:7000/dist'
    },
    externals: {
        'cherio': 'window',
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
    },
    module: {
        preLoaders: [
             {
                 test: /\.jsx$/,
                 loaders: ['eslint', 'jscs'],
                 include: path.join(__dirname, 'lib')
             }
        ],
        loaders: [
            {
                test: /\.jsx?$/,
                exlude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['react', 'es2015', 'stage-0']
                }
            }
        ]
    },
    plugins: []
};

module.exports = config;
