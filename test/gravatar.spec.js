import React from 'react';
import {mount, shallow} from 'enzyme';
import {expect} from 'chai';
import md5 from 'md5';

import Avatar from '../lib/avatar';
import Email from '../lib/email';
import Gravatar from '../lib/gravatar';

describe('<Gravatar />', () => {
    it('contains an <Avatar/> component', function() {
        const wrapper = mount(<Gravatar />);
        expect(wrapper.find(Avatar)).to.have.length(1);
    });
    it('contains an <Email/> component', function() {
        const wrapper = mount(<Gravatar />);
        expect(wrapper.find(Email)).to.have.length(1);
    });
    it('should have initial email state', function() {
        const wrapper = mount(<Gravatar />);
        expect(wrapper.state().email).to.equal('someone@example.com');
    });
    it('shoudl have initial src state', function() {
        const wrapper = mount(<Gravatar />);
        expect(wrapper.state().src).to.equal('http://placehold.it/200x200');
    });
    it('it should update the src state on clicking fetch', function() {
        const wrapper = mount(<Gravatar />);
        const email = 'hello@ifelse.io';
        wrapper.setState({email: email});
        wrapper.find('button').simulate('click');
        expect(wrapper.state('email')).to.equal(email);
        expect(wrapper.state('src')).to.equal(`http://gravatar.com/avatar/${md5(email)}?s=200`)
    });
});