import React from 'react';
import {mount, shallow} from 'enzyme';
import {expect} from 'chai';

import Email from '../lib/email';

describe('<Email/>', function() {
    it('should have input for email', function() {
        const wrapper = shallow(<Email />);
        expect(wrapper.find('input')).to.have.length(1);
    });
    it('should have a single button', function() {
        const wrapper = shallow(<Email />);
        expect(wrapper.find('button')).to.have.length(1);
    });
    it('should have props for handelEmailChange and fetchGravatar', function() {
        const wrapper = shallow(<Email />);
        expect(wrapper.props().handleEmailChange).to.be.defined;
        expect(wrapper.props().fetchGravatar).to.be.defined;
    });
});
